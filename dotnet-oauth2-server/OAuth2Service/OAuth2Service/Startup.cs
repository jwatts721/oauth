﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Infrastructure;
using Microsoft.Owin.Security.OAuth;
using Owin;

[assembly: OwinStartup(typeof(OAuth20.Startup))]

namespace OAuth20
{
    public class Startup
    {
        public static OAuthAuthorizationServerOptions OAuthServerOptions { get; private set; }

        public void Configuration(IAppBuilder app)
        {
            // OAuth2 Configuration
            OAuthServerOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/token"),
                Provider = new OAuthAuthorizationServerProvider
                {
                    OnValidateClientAuthentication = async context =>
                    {
                        await Task.Run(() => context.Validated());
                    },
                    OnGrantResourceOwnerCredentials = async context =>
                    {
                        await Task.Run(() =>
                        {
                            using (var entities = new OAuth2DBEntities())
                            {
                                var client =
                                    entities.oauth_clients.FirstOrDefault(
                                        w => w.client_id == context.UserName && w.client_secret == context.Password);
                                if (client == null)
                                {
                                    context.Rejected();
                                    return;
                                }
                            }

                            var oAuthIdentity = new ClaimsIdentity(context.Options.AuthenticationType);
                            context.Validated(oAuthIdentity);
                        });
                    },
                    OnGrantRefreshToken = async context =>
                    {
                        await Task.Run(() =>
                        {
                            context.Validated();
                        });
                    },
                },
                RefreshTokenProvider = new SimpleRefreshTokenProvider(),
                AllowInsecureHttp = true,
                AccessTokenExpireTimeSpan = TimeSpan.FromHours(1)
            };
            
            app.UseOAuthBearerTokens(OAuthServerOptions);
            app.UseCors(CorsOptions.AllowAll);
            

            // Web API Self Hosted configuration
            var apiConfig = new HttpConfiguration();
            apiConfig.Routes.MapHttpRoute(
                    name: "DefaultApi",
                    routeTemplate: "api/{controller}/{id}",
                    defaults: new { id = RouteParameter.Optional }
                );
            app.UseWebApi(apiConfig);
        }
    }

    public class SimpleRefreshTokenProvider : IAuthenticationTokenProvider
    {
        private static readonly ConcurrentDictionary<string, AuthenticationTicket> RefreshTokens = new ConcurrentDictionary<string, AuthenticationTicket>();

        public void Create(AuthenticationTokenCreateContext context)
        {
            throw new NotImplementedException();
        }

        public async Task CreateAsync(AuthenticationTokenCreateContext context)
        {
            await Task.Run(() =>
            {
                var guid = Guid.NewGuid().ToString();

                // Set the expiration of the ticket
                context.Ticket.Properties.ExpiresUtc = DateTime.UtcNow.AddDays(1);

                //saving the new refreshTokenTicket to a local var of Type ConcurrentDictionary<string,AuthenticationTicket>
                // consider storing only the hash of the handle
                RefreshTokens.TryAdd(guid, context.Ticket); // TODO: alternative to TryAdd for 30 day refresh tokens
                context.SetToken(guid);
            });
        }

        public void Receive(AuthenticationTokenReceiveContext context)
        {
            throw new NotImplementedException();
        }

        public async Task ReceiveAsync(AuthenticationTokenReceiveContext context)
        {
            await Task.Run(() =>
            {
                AuthenticationTicket ticket;
                if (RefreshTokens.TryGetValue(context.Token, out ticket))
                {
                    context.SetTicket(ticket);
                }
                //if (RefreshTokens.TryRemove(context.Token, out ticket))
                //{
                //    context.SetTicket(ticket);
                //}
            });
        }
    }
}
