//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OAuth20
{
    using System;
    using System.Collections.Generic;
    
    public partial class oauth_scopes
    {
        public int id { get; set; }
        public string scope { get; set; }
        public Nullable<bool> is_default { get; set; }
    }
}
