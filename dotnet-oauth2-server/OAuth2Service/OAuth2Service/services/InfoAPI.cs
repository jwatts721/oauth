﻿using System.Web;
using System.Web.Http;

namespace OAuth20.services
{
    [Authorize]
    public class InfoController : ApiController
    {
        public UserRecord GetUserRecord()
        {
            return new UserRecord
            {
                Id = 1,
                FirstName = "James",
                LastName = "Dokes",
                Active = true,
                Info = "Additional Info: " + HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host +
                    ":" + HttpContext.Current.Request.Url.Port
            };
        }

    }

    public class UserRecord
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool Active { get; set; }
        public string Info { get; set; }
    }
}