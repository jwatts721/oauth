$(function () {
    displayLogin();

    function displayLogin() {
        $.get('templates/login.html', function (html) {
            $('#content').html(html);

            // Binding Buttons
            $('#login-button').click(function () {
                if (!$('#username').val() || !$('#password').val()) {
                    displayModalAlert({ 'header': 'Credentials Required', 'text': 'A username and password is required!' });
                    return;
                }
                credentials.username = $('#username').val();
                credentials.password = $('#password').val();

                //callAuthorizationService();
                // Now execute call to retreive the access token
                var tokenRequest = {
                    'username': credentials.username,
                    'password': credentials.password,
                    'grant_type': 'password'
                };
                callTokenService(tokenRequest);
            });
        });
    }

    function displayHome(data) {
        $.get('templates/home.html', function (html) {
            html = html.setData('ClientId', credentials.username);
            html = html.setData('AccessToken', data.access_token);
            html = html.setData('Expiration', data.expires_in);
            html = html.setData('TokenType', data.token_type);
            $('#content').html(html);
        });
    }

    function displayModalAlert(modalData) {
        $.get('templates/modal-alert.html', function (maHtml) {
            var maContent = maHtml;
            maContent = maContent.setData('Header', modalData.header);
            maContent = maContent.setData('ModalText', modalData.text);
            $('#modal-alert-container').html(maContent);
            $('#modal-alert').modal();
        });
    }

    function callAuthorizationService() {
        // Call authorization page
        $.get('/authorize?response_type=code&client_id=' + credentials.username + '&state=XYZ', function (data) {
            var response = JSON.parse(data);
            if (typeof (response.error) !== 'undefined') {
                displayModalAlert({ 'header': response.error.toUpperCase(), 'text': response.error_description });
                return;
            }
            $('#content').html(response.html);

            // Binding for buttons on HTML response
            $('#authorized_yes').click(function () {
                var authRequest = {
                    'authorized': 'yes',
                    'response_type': 'code',
                    'client_id': credentials.username,
                    'state': 'XYZ'
                };
                $.post('/authorize', authRequest, function (data, status) {
                    var response = JSON.parse(data);
                    // Now execute call to retreive the access token
                    var tokenRequest = {
                        'client_id': credentials.username,
                        'client_secret': credentials.password,
                        'grant_type': 'authorization_code',
                        'code': response.auth_code
                    };
                    callTokenService(tokenRequest);
                });
                return false;
            });
            $('#authorized_no').click(function () {
                $.post('/authorize', { 'authorized': 'no' }, function (data, status) {
                    displayModalAlert({ 'header': 'Access Denied', 'text': 'The user denied access to the application!' });
                    displayLogin();
                });
                return false;
            });
        }).fail(function (data) {
            var response = JSON.parse(data.responseText);
            displayModalAlert({ 'header': response.error.toUpperCase(), 'text': response.error_description });
        });
    }

    function callTokenService(tokenRequest) {
        $.post('/token', tokenRequest, function (data, status) {
            displayHome(data);

            //callProtectedResource(data.access_token);
        }).fail(function (data) {
            var response = JSON.parse(data.responseText);
            
            displayModalAlert({ 'header': response.error.toUpperCase(), 'text': response.error });
            displayLogin();
        });
    }

    function callProtectedResource(accessToken) {
        // Now call the protected resource using the access token 
        $.post('/resource', { 'access_token': accessToken }, function (data) {
            var response = JSON.parse(data);
            $('#resource-data').html('<h3>' + response.message + '</h3>');
        });
    }
});

String.prototype.setData = function (placeholder, data) {
    return this.replace('{{' + placeholder + '}}', data);
};

var credentials = {
    username: '', password: ''
};
var authInfo = {
    client_id: '',
    access_token: '',
    expiration: '',
    refresh_token: '',
    token_type: '',
    scope: ''
};