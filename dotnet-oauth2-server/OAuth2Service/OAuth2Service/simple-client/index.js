﻿var USERNAME = "testclient";
var PASSWORD = "testpass";

function authorize() {
    var loginData = {
        "grant_type": "password",
        "username": USERNAME,
        "password": PASSWORD
    };

    $.post("/token", loginData, function(data) {
        console.log(data);
        var output = "<p>Access Token: " + data.access_token + "</p>";
        output += "<p>Expires: " + data.expires_in + "</p>";
        output += "<p>Token Type: " + data.token_type + "</p>";
        output += "<p>Refresh Token: " + data.refresh_token + "</p>";

        // Update session storage
        sessionStorage.setItem("access_token", data.access_token);
        sessionStorage.setItem("refresh_token", data.refresh_token);

        $(".response-data").html(output);
    }).fail(function(data) {
        console.log(data);
        var output = "<h2>" + data.statusText + "</h2>";
        output += "<h3>" + JSON.parse(data.responseText).error + "</h3>";

        $(".response-data").html(output);

        sessionStorage.clear();
    });
}

function getProtectedData() {
    if (sessionStorage.getItem("access_token") === null) {
        alert("Please click Get Token first in order to set the access token");
        return;
    }

    $.ajax({
        url: "/api/Info",
        type: "GET",
        dataType: "json",
        headers: { "Authorization":"Bearer " + sessionStorage.getItem("access_token") },
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            console.log(data);
            var output = "<p>First Name: " + data.FirstName + "</p>";
            output += "<p>Last Name: " + data.LastName + "</p>";
            output += "<p>ID: " + data.Id + "</p>";
            output += "<p>Active: " + data.Active + "</p>";
            output += "<p>Info: " + data.Info + "</p>";

            $(".response-data").html(output);
        },
        error: function(error) {
            console.log(error);
            var output = "<h2>" + error.statusText + "</h2>";
            output += "<h3>" + JSON.parse(error.responseText).Message + "</h3>";

            $(".response-data").html(output);

            sessionStorage.clear();
        }
    });
}