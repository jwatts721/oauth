﻿function refresh() {

    if (sessionStorage.getItem("refresh_token") === null) {
        alert("Please click Get Token first in order to set the refresh token");
        return;
    }

     var refreshData = {
        "grant_type": "refresh_token",
        "refresh_token": sessionStorage.getItem("refresh_token")
     };

    console.log(refreshData);

    $.post("/token", refreshData, function (data) {
        console.log(data);
        var output = "<p>Access Token: " + data.access_token + "</p>";
        output += "<p>Expires: " + data.expires_in + "</p>";
        output += "<p>Token Type: " + data.token_type + "</p>";
        output += "<p>Refresh Token: " + data.refresh_token + "</p>";
        
        $(".response-data").html(output);
    }).fail(function (data) {
        console.log(data);
        var output = "<h2>" + data.statusText + "</h2>";
        output += "<h3>" + JSON.parse(data.responseText).error + "</h3>";

        $(".response-data").html(output);

        sessionStorage.clear();
    });
}

function refreshWithBadData() {

    var refreshData = {
        "grant_type": "refresh_token",
        "client_id": USERNAME,
        "refresh_token": "170c223b-5e07-40b0-8c66-ba6585e5cf90"
    };

    $.post("/token", refreshData, function (data) {
        var output = "<p>Access Token: " + data.access_token + "</p>";
        output += "<p>Expires: " + data.expires_in + "</p>";
        output += "<p>Token Type: " + data.token_type + "</p>";
        output += "<p>Refresh Token: " + data.refresh_token + "</p>";

        $(".response-data").html(output);
    }).fail(function (data) {
        console.log(data);
        var output = "<h2>" + data.statusText + "</h2>";
        output += "<h3>" + JSON.parse(data.responseText).error + "</h3>";

        $(".response-data").html(output);

        sessionStorage.clear();
    });
}

