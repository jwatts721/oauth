<?php

// include our OAuth2 Server object
require_once __DIR__.'/server.php';

$request = OAuth2\Request::createFromGlobals();
$response = new OAuth2\Response();

// validate the authorize request
if (!$server->validateAuthorizeRequest($request, $response)) {
    //$response->send();
    echo json_encode(array('error' => 'invalid_request', 'error_description' => 'Client ID or parameter invalid!'));
    die;
}
// display an authorization form
if (empty(filter_input(INPUT_POST, 'authorized'))) {
  exit(json_encode(array('html' => '
<form>
  <label>Do You Authorize TestClient?</label><br />
  <button id="authorized_yes" class="btn btn-success">Yes</button>
  <button id="authorized_no" class="btn btn-danger">No</button>
</form>')));
}

// print the authorization code if the user has authorized your client
$is_authorized = filter_input(INPUT_POST, 'authorized', FILTER_DEFAULT) === 'yes';
$server->handleAuthorizeRequest($request, $response, $is_authorized);
if ($is_authorized) {
  // this is only here so that you get to see your code in the cURL request. Otherwise, we'd redirect back to the client
  $code = substr($response->getHttpHeader('Location'), strpos($response->getHttpHeader('Location'), 'code=')+5, 40);
  exit(json_encode(array('auth_code' => $code)));
}
echo(json_encode(array('error' => 'access_denied', 'error_description' => 'The user denied access to your application')));
